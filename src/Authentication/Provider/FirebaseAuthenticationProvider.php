<?php

namespace Drupal\firebase\Authentication\Provider;

use Drupal\Core\Authentication\AuthenticationProviderInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Drupal\Core\Path\PathMatcherInterface;
use Drupal\Core\Path\CurrentPathStack;
use Firebase\JWT\JWT;
use Drupal\user\Entity\User;

/**
 * Class FirebaseAuthenticationProvider.
 *
 * @package Drupal\firebase\Authentication\Provider
 */
class FirebaseAuthenticationProvider implements AuthenticationProviderInterface {
	
	/**
	 * The config factory.
	 *
	 * @var \Drupal\Core\Config\ConfigFactoryInterface
	 */
	protected $configFactory;
	
	/**
	 * The entity type manager.
	 *
	 * @var \Drupal\Core\Entity\EntityTypeManagerInterface
	 */
	protected $entityTypeManager;
	
	/**
	 * Path Matcher
	 *
	 * @var \Drupal\Core\Path\PathMatcherInterface
	 */
	protected $pathMatcher;
	
	/**
	 * Current Path
	 *
	 * @var \Drupal\Core\Path\CurrentPathStack
	 */
	protected $currentPath;
	
	/**
	 * Constructs a HTTP basic authentication provider object.
	 *
	 * @param ConfigFactoryInterface $config_factory        	
	 * @param EntityTypeManagerInterface $entity_type_manager        	
	 * @param CurrentPathStack $currentPath        	
	 * @param PathMatcherInterface $pathMatcher        	
	 */
	public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, CurrentPathStack $currentPath, PathMatcherInterface $pathMatcher) {
		$this->configFactory = $config_factory;
		$this->entityTypeManager = $entity_type_manager;
		$this->currentPath = $currentPath;
		$this->pathMatcher = $pathMatcher;
	}
	
	/**
	 * Checks whether suitable authentication credentials are on the request.
	 *
	 * @param \Symfony\Component\HttpFoundation\Request $request
	 *        	The request object.
	 *        	
	 * @return bool TRUE if authentication credentials suitable for this provider are on the
	 *         request, FALSE otherwise.
	 */
	public function applies(Request $request) {
		// If you return TRUE and the method Authentication logic fails,
		// you will get out from Drupal navigation if you are logged in.
		return $this->pathMatcher->matchPath ( $this->currentPath->getPath (), '/api/*' );
	}
	
	/**
	 *
	 * {@inheritdoc}
	 *
	 */
	public function authenticate(Request $request) {
		if ($request->headers->has ( 'authorization' )) {
			
			$matches = array ();
			$regex = '/^Bearer ((?<header>[a-zA-Z0-9\-_]+)\.(?<payload>[a-zA-Z0-9\-_]+)\.(?<signature>[a-zA-Z0-9\-_]+))$/';
			
			if (preg_match ( $regex, $request->headers->get ( 'authorization' ), $matches )) {
				$idToken = $matches [1];
				$identity = [ ];
				
				$uri = 'https://www.googleapis.com/robot/v1/metadata/x509/securetoken@system.gserviceaccount.com';
				$certsRes = \Drupal::httpClient ()->get ( $uri );
				$certs = json_decode ( $certsRes->getBody () );
				
				try {
					$identity = JWT::decode ( $idToken, ( array ) $certs, [ 
							'RS256' 
					] );
					
					// TODO: check if audience is 'semilla-besada-61d33' (firebase project_id in service account json)
					// TODO: check if issuer is 'https://securetoken.google.com/semilla-besada-61d33'
					// TODO: cache public key, and store it for next @author remi
					// if auth fails, refresh key
				} catch ( \DomainException $e ) {
					echo 'Domain error: ' . $e->getMessage ();
				} catch ( \UnexpectedValueException $e ) {
					echo 'Unexcpetced error: ' . $e->getMessage ();
				} catch ( \SignatureInvalidException $e ) {
					echo 'Invalid error: ' . $e->getMessage ();
				} catch ( \BeforeValidException $e ) {
					echo 'error: ' . $e->getMessage ();
				} catch ( \ExpiredException $e ) {
					echo 'Expired error: ' . $e->getMessage ();
				}																
				
				if ($user = reset($this->entityTypeManager->getStorage ( 'user' )->loadByProperties ( [ 
						'mail' => $identity->email 
				] ))) {
					return $user;
				}
			}
		}
		
		throw new AccessDeniedHttpException ();
	}
	
	/**
	 *
	 * {@inheritdoc}
	 *
	 */
	public function cleanup(Request $request) {
	}
	
	/**
	 *
	 * {@inheritdoc}
	 *
	 */
	public function handleException(GetResponseForExceptionEvent $event) {
		$exception = $event->getException ();
		if ($exception instanceof AccessDeniedHttpException) {
			$event->setException ( new UnauthorizedHttpException ( 'Invalid consumer origin.', $exception ) );
			return TRUE;
		}
		return FALSE;
	}
}
