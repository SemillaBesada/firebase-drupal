<?php

namespace Drupal\firebase\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Facebook\Facebook;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Firebase\JWT\JWT;
use Drupal\user\Entity\User;
use Drupal\Core\Url;

class FirebaseLoginController extends ControllerBase {
	
	/**
	 * Facebook OAuth2 Client with app id and secret
	 *
	 * @return \Facebook\Facebook
	 */
	private function getFacebookClient() {
		$settings = $this->config ( 'firebase.settings' )->get ( 'apps.facebook' );
		return new Facebook ( [ 
				'app_id' => intval ( $settings ['app_id'] ),
				'app_secret' => $settings ['app_secret'],
				'default_graph_version' => 'v2.8' 
		] );
	}
	
	/**
	 * Google OAuth2 Client with firebase app json config
	 *
	 * @return \Google_Client
	 */
	private function getGoogleClient() {
		$settings = $this->config ( 'firebase.settings' );
		$appConfigPath = $settings->get ( 'app_json_filepath' );
		$scopes = [ 
				'openid',
				'https://www.googleapis.com/auth/plus.me',
				'https://www.googleapis.com/auth/userinfo.email',
				'https://www.googleapis.com/auth/userinfo.profile' 
		];
		$callbackUrl = Url::fromRoute ( 'firebase.callback', [ 
				'connection' => 'google' 
		] );
		$redirectUri = $callbackUrl->setAbsolute ()->toString ( TRUE );
		
		if (! isset ( $_SESSION ['state'] )) {
			$_SESSION ['state'] = uniqid ();
		}
		
		$client = new \Google_Client ();
		$client->setAuthConfig ( $appConfigPath );		
		$client->setScopes ( $scopes );
		$client->setState ( $_SESSION ['state'] );
		$client->setRedirectUri ( $redirectUri->getGeneratedUrl () );
		
		return $client;
	}
	
	/**
	 * Returns the response for the firebase app,
	 * containing the custom token
	 *
	 * @param array $profile
	 * @return \Symfony\Component\HttpFoundation\JsonResponse
	 */
	private function getFirebaseResponse(array $profile) {
		$user = null;
	
		try {
			$user = $this->loadIdentity ( $profile );
		} catch ( Exception $e ) {
			return new JsonResponse ( [
					'error' => $e->getMessage()
			] );
		}
	
		$uid = $user->id ();
	
		return new JsonResponse ( [
				'account name' => $user->getAccountName (),
				'display name' => $user->getDisplayName (),
				'email' => $user->getEmail (),
				'uid' => $uid,
				'token' => $this->getFirebaseToken ( $uid )
		] );
	}
	
	/**
	 *
	 * @param array $identity
	 * @throws Exception
	 * @return \Drupal\user\Entity\User
	 */
	private function loadIdentity(array $identity) {
		if ($user = user_load_by_mail ( $identity ['email'] )) {
			return $user;
		} elseif (isset ( $identity ['email'] ) && isset ( $identity ['name'] )) {
			$user = User::create ();
				
			$user->setEmail ( $identity ['email'] );
			$user->setPassword ( uniqid().user_password ( 16 ) );
			$user->setUsername ( $identity ['name'] );
			$user->enforceIsNew ();
			$user->save ();
				
			return $user;
		}
	
		throw new HttpException ( 500, 'Error loading profile' );
	}
	
	/**
	 * generates custom token
	 *
	 * uid: user id
	 * claims: assoc. array
	 *
	 * @param unknown $uid        	
	 * @param array $claims        	
	 * @return string
	 */
	private function getFirebaseToken($uid, $claims = [] ) {
		$settings = $this->config ( 'firebase.settings' );
		$serviceConfigPath = $settings->get ( 'service_json_filepath' );
		$config = json_decode ( file_get_contents ( $serviceConfigPath ) );
		
		$service_account_email = $config->client_email;
		$private_key = $config->private_key;
		
		$claims += [				
				'provider' => 'custom',
				'name'=> 'bobbie',				
				'website' => 'http://remirules.com',
				'picture'=> 'https://lh3.googleusercontent.com/-SvTgeprtO-s/AAAAAAAAAAI/AAAAAAAAAAA/AEMOYSDRB4Wt7iiN-mbOMwbwjt-DRtqVag/s96-c/photo.jpg',
				'email'=> 'bobby@kaka.com',
				'email_verified' => true,				
		];
		
		$now_seconds = time ();
		$payload = array (
				'iss' => $service_account_email,
				'sub' => $service_account_email,
				'aud' => 'https://identitytoolkit.googleapis.com/google.identity.identitytoolkit.v1.IdentityToolkit',
				'iat' => $now_seconds,
				'exp' => $now_seconds + (60 * 60), // Maximum expiration time is one hour
				'uid' => $uid,				
				'claims' => $claims, 
		);
		
		return JWT::encode ( $payload, $private_key, 'RS256' );
	}
	
	/**
	 * Log in with the specified connection
	 *
	 * @param Request $request        	
	 * @param unknown $connection        	
	 * @throws HttpException
	 * @return \Drupal\Core\Routing\TrustedRedirectResponse
	 */
	public function login(Request $request, $connection) {
		session_reset();
		
		switch ($connection) {
			case 'facebook' :
				return $this->redirectToFacebookLogin ( $request );
			case 'google' :
				return $this->redirectToGoogleLogin ( $request );
				break;
		}
		
		throw new AccessDeniedHttpException ( 'elaba matj!' );
	}
	
	/**
	 * Callbacks for logins
	 *
	 * @param Request $request        	
	 * @param unknown $connection        	
	 * @throws HttpException
	 * @return \Symfony\Component\HttpFoundation\JsonResponse
	 */
	public function callback(Request $request, $connection) {
		switch ($connection) {
			case 'facebook' :
				return $this->callbackFacebookLogin ( $request );
			case 'google' :
				return $this->callbackGoogleLogin ( $request );
				break;
		}
		
		throw new HttpException ( 403, 'kaka' );
	}
	
	/**
	 * Redirects to the facebook login widget
	 *
	 * @param Request $request        	
	 * @return \Drupal\Core\Routing\TrustedRedirectResponse
	 */
	private function redirectToFacebookLogin(Request $request) {
		session_reset ();
		session_start ();
		$widgetUrl = '<front>';
		$scopes = [ 
				'email',
				'user_friends' 
		];
		
		try {
			$fb = $this->getFacebookClient ();
			$helper = $fb->getRedirectLoginHelper ();
			$state = uniqid ();
			$helper->getPersistentDataHandler ()->set ( 'state', $state );
			
			$redirectRoute = Url::fromRoute ( 'firebase.callback', [ 
					'connection' => 'facebook' 
			] );
			
			$redirectUrl = $redirectRoute->setAbsolute ()->toString ( TRUE )->getGeneratedUrl ();
			
			$widgetUrl = $helper->getLoginUrl ( $redirectUrl, $scopes );
		} catch ( FacebookSDKException $e ) {
			session_abort();
			throw new AccessDeniedHttpException ();
		}
		
		return new TrustedRedirectResponse ( $widgetUrl );
	}
	
	/**
	 * Redirects to the google login widget
	 *
	 * @param Request $request        	
	 * @return \Drupal\Core\Routing\TrustedRedirectResponse
	 */
	private function redirectToGoogleLogin(Request $request) {
		session_reset ();
		session_start ();
		
		$client = $this->getGoogleClient ();
		$url = $client->createAuthUrl ();
		
		return new TrustedRedirectResponse ( $url );
	}
	
	/**
	 * Handler for Facebook login callbacks
	 *
	 * @param Request $request        	
	 * @return \Symfony\Component\HttpFoundation\JsonResponse
	 */
	private function callbackFacebookLogin(Request $request) {
		//TODO: catch facebook exceptions
		session_start ();
		
		$fb = $this->getFacebookClient ();
		$helper = $fb->getRedirectLoginHelper ();
		$token = $helper->getAccessToken ();
		$fields = 'id,cover,name,first_name,last_name,age_range,link,gender,locale,picture,timezone,updated_time,verified,email';
		$res = $fb->get ( '/me?fields=' . $fields, $token );
		$profile = $res->getGraphNode ()->asArray ();
		
		return $this->getFirebaseResponse ( $profile );
	}
	
	/**
	 * Handler for google login callbacks
	 *
	 * @param Request $request        	
	 * @return \Symfony\Component\HttpFoundation\JsonResponse
	 */
	private function callbackGoogleLogin(Request $request) {
		session_start ();
		
		if ($_SESSION ['state'] !== $request->query->get ( 'state' )) {
			return new JsonResponse ( [ 
					'error' => 'state invalid' 
			] );
		}
		
		$google = $this->getGoogleClient ();
		$accesToken = $google->authenticate ( $request->query->get ( 'code' ) );
		$profile = $google->verifyIdToken ( $accesToken ['id_token'] );
		
// 		var_dump($profile);
// 		return;
		
		return $this->getFirebaseResponse ( $profile );
	}		
}