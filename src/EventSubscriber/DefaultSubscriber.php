<?php

namespace Drupal\firebase\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\Event;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Path\PathMatcher;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;

/**
 * Class DefaultSubscriber.
 *
 * @package Drupal\firebase
 */
class DefaultSubscriber implements EventSubscriberInterface {

  /**
   * Drupal\Core\Path\CurrentPathStack definition.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected $pathCurrent;
  /**
   * Drupal\Core\Path\PathMatcher definition.
   *
   * @var \Drupal\Core\Path\PathMatcher
   */
  protected $pathMatcher;

  /**
   * Constructor.
   */
  public function __construct(CurrentPathStack $path_current, PathMatcher $path_matcher) {
    $this->pathCurrent = $path_current;
    $this->pathMatcher = $path_matcher;
  }

  /**
   * {@inheritdoc}
   */
  static function getSubscribedEvents() {
  	$events = [];
    $events['kernel.response'] = ['onResponseCreation'];

    return $events;
  }

  /**
   * This method is called whenever the kernel.response event is
   * dispatched.
   *
   * @param GetResponseEvent $event
   */
  public function onResponseCreation(FilterResponseEvent $event) {
    drupal_set_message($event->getRequest()->headers->get('host'), 'status', TRUE);
    
    $request = $event->getRequest();
    $response = $event->getResponse();
    
    $currentPath = $this->pathCurrent->getPath($request);    
    $isCorsPath = $this->pathMatcher->matchPath($currentPath, '/api/*');
    
    if($isCorsPath) {
    	if ($request->getMethod () === 'OPTIONS') {
    		$allowedMethods = 'GET,POST,OPTIONS';
    		if (strlen ( $allowedMethods ) > 0) {
    			$response->headers->add ( array (
    					'Access-Control-Allow-Methods' => $allowedMethods,
    					'Access-Control-Allow-Headers' => 'Content-Type, authorization, X-CSRF-Token, accept'
    			) );
    		}
    	} if ($request->headers->has ( 'origin' ) && ($response->headers->get('origin') !== $request->headers->get('origin'))) {
    		$response->headers->add ( array (
    				'Access-Control-Allow-Origin' => $request->headers->get ( 'origin', $response->headers->get ( 'origin' ) ),
    				'Access-Control-Allow-Headers' => 'Content-Type, authorization, X-CSRF-Token, accept',    				
    		) );
    	}
    }
  }

}
