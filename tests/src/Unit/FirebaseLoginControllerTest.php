<?php

namespace Drupal\Tests\firebase\Unit;

use Drupal\Tests\UnitTestCase;
use Symfony\Component\HttpFoundation\Request;
use Drupal\firebase\Controller\FirebaseLoginController;

/**
 * Tests the login controller
 *
 * @group firebase
 * 
 * @author remi
 *        
 */
class FirebaseLoginControllerTest extends UnitTestCase {
	protected function setUp() {
		parent::setUp ();
	}
	
	/**
	 * a hello phpunit
	 */
	public function testLogin() {
		$request = new Request ();
		$controller = new FirebaseLoginController ();
		
		$redirectUrl = $controller->login ( $request, 'facebook' );
		
		$this->assertEquals ( $redirectUrl, true );
	}
}